@extends('layout')

@section('content')

    <style>
        .container {
            max-width: 450px;
        }
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="card push-top">
        <div class="card-header">
            Edit & Update
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('cars.update', $car->id) }}">
                <div class="form-group">
                    @csrf
                    @method('PATCH')
                    <label for="registration">Registration</label>
                    <input type="text" class="form-control" name="registration" value="{{ $car->registration }}"/>
                </div>
                <div class="form-group">
                    <label for="brand">Brand</label>
                    <input type="text" class="form-control" name="brand" value="{{ $car->brand }}"/>
                </div>
                <div class="form-group">
                    <label for="model">Model</label>
                    <input type="text" class="form-control" name="model" value="{{ $car->model }}"/>
                </div>
                <div class="form-group">
                    <label for="engine">Engine</label>
                    <input type="text" class="form-control" name="engine" value="{{ $car->engine }}"/>
                </div>
                <button type="submit" class="btn btn-block btn-danger">Update Car</button>
            </form>
        </div>
    </div>
@endsection
