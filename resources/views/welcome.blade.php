@extends('layout')

@section('content')

    <style>
        .container {
            max-width: 450px;
        }
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="card-header">
        CARS STOCK WEB
    </div>
    <div class="push-top">
        <a href="{{ route('cars.create')}}" class="btn btn-primary btn-sm">Create car</a>
        <a href="{{ route('cars.index')}}" class="btn btn-primary btn-sm">Cars list</a>
    </div>
@endsection
