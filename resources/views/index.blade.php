@extends('layout')

@section('content')

    <style>
        .push-top {
            margin-top: 50px;
        }
    </style>

    <div class="card-header">
        Cars list
    </div>
    <div class="push-top">
        <table class="table">
            <thead>
            <tr class="table-warning">
                <td>ID</td>
                <td>Registration</td>
                <td>Brand</td>
                <td>Model</td>
                <td>Engine</td>
                <td class="text-center">Action</td>
            </tr>
            </thead>
            <tbody>
            @foreach($car as $cars)
                <tr>
                    <td>{{$cars->id}}</td>
                    <td>{{$cars->registration}}</td>
                    <td>{{$cars->brand}}</td>
                    <td>{{$cars->model}}</td>
                    <td>{{$cars->engine}}</td>
                    <td class="text-center">
                        <a href="{{ route('cars.edit', $cars->id)}}" class="btn btn-primary btn-sm">Edit</a>
                        <form action="{{ route('cars.destroy', $cars->id)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
