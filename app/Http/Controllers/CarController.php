<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;


class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car = Cars::all();
        return view('index', compact('car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carData = $request->validate([
            'registration' => 'required|max:20',
            'brand' => 'required|max:50',
            'model' => 'required|max:50',
            'engine' => 'required|max:20'
        ]);

        $car = Cars::create($carData);

        return redirect('/cars')->with('completed', 'Car has been saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**f
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Cars::findOrFail($id);

        return view('edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = $request->validate([
            'registration' => 'required|max:20',
            'brand' => 'required|max:50',
            'model' => 'required|max:50',
            'engine' => 'required|max:20'
        ]);

        Cars::whereId($id)->update($updateData);

        return redirect('/cars')->with('completed', 'Car has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Cars::findOrFail($id);
        $car->delete();

        return redirect('/cars')->with('completed', 'Car has been deleted');
    }
}
